﻿namespace BrightSword.Pegasus.API.Enumerations
{
    public enum CompletionStatus
    {
        Pending,

        Queued,

        Completed,

        Warning_NoHandlerSpecified,

        Error_HandlerCouldNotBeLoaded,

        Error_HandlerNotInvokedSuccessfully,

        Error_ExceptionWhileExecution
    }
}