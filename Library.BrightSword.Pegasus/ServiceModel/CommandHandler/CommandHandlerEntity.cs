﻿using System;
using System.IO;

using BrightSword.Pegasus.API;

using Microsoft.WindowsAzure.Storage.Table;

namespace BrightSword.Pegasus.ServiceModel.CommandHandler
{
    public class CommandHandlerEntity : TableEntity
    {
        public CommandHandlerEntity()
        {
            PartitionKey = "TaskHandlers";
            Timestamp = DateTime.Now;
        }

        public CommandHandlerEntity(string commandName,
                                    Type commandHandlerType)
            : this()
        {
            CommandName = commandName;
            CommandHandlerTypeAssembly = Path.GetFileNameWithoutExtension(commandHandlerType.Assembly.CodeBase);
            CommandHandlerTypeAssemblyQualifiedName = commandHandlerType.FullName;
        }

        public CommandHandlerEntity(string commandName,
                                    string commandHandlerTypeAssembly,
                                    string commandHandlerTypeAssemblyQualifiedName)
            : this()
        {
            CommandName = commandName;
            CommandHandlerTypeAssembly = commandHandlerTypeAssembly;
            CommandHandlerTypeAssemblyQualifiedName = commandHandlerTypeAssemblyQualifiedName;
        }

        public string CommandName
        {
            get { return RowKey; }
            set { RowKey = value; }
        }

        public string CommandHandlerTypeAssembly { get; set; }

        public string CommandHandlerTypeAssemblyQualifiedName { get; set; }

        public void ProcessCommand(ICommand command,
                                   ICommandHandlerContext context) {}
    }
}