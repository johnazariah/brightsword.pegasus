﻿Feature: DownloadHandlers
	In order to dynamically upgrade the system
	I want to be able to download a task handler from the cloud
	So that I can handle tasks that I could not before

Background: 
	Given A CloudRunner Storage Configuration of
		| RegisteredTasksTableName | ScheduledTasksQueueName          | RegisteredHandlersTableName | RegisteredHandlersBlobContainerName | StorageAccount | StorageAccountKey                                                                        | RuntimeErrorsTableName   |
		| CloudRunnerTasks         | cloudrunner-scheduled-task-queue | CloudRunnerHandlers         | cloudrunner-assemblies              | myobpdqaart    | MHDYieseAgGX6HbE8yWK4e0L2eKkUt+an6UbX1/6o0nC2FK2pmgxi/OVW+WEVyH50ROBRMtw0wxf057KDTaDfA== | CloudRunnerRuntimeErrors |

	And An empty ScheduledTasksQueue

	#And An empty RegisteredHandlersTable

	#And An empty RegisteredHandlersBlobContainer

	#And The "Test.Handlers.dll" assembly is uploaded to the RegisteredHandlersBlobContainer

	#And A TaskHandler is inserted into RegisteredHandlersTable
	#	| HandlerName | HandlerTypeAssemblyQualifiedName | HandlerAssembly   |
	#	| Dummy       | Test.Handlers.NullTaskHandler    | Test.Handlers.dll |

Scenario: Downloading a new task handler	
	Given An empty RegisteredTasksTable

	And No "Test.Handlers" assembly available to load

	And A CloudRunnerTask is inserted into RegisteredTasksTable
		| PartitionKey | RowKey | ETag | Name   | TaskHandlerName | TaskInputParameters  | TaskOutputParameters | NextScheduledRuntime | CompletionStatus |
		| test_data    | 100    | *    | Sample | Dummy            | {text:"Hello World"} |                      | 1601-01-01T00:00:00  | Pending          |

	When Sample has a NextScheduledRuntime in the "past" by "02:00:00:00.0000000"
	And the TaskScheduler invokes the enqueue operation on CloudRunnerTasks
	Then A ScheduledTask must be enqueued to ScheduledCloudRunnerTasks
	And Sample must have a CompletionStatus of "Queued"

	When A ScheduledTask is dequeued from ScheduledCloudRunnerTasks
	And the TaskProcess invokes the process operation on the ScheduledTask
	Then the "Test.Handlers" assembly should be downloaded
	Then The ScheduledTask should not exist in ScheduledCloudRunnerTasks any more
	And Sample must have a CompletionStatus of "Completed"

