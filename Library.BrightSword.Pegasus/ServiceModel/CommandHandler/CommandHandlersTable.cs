﻿using BrightSword.Pegasus.Core;
using BrightSword.Pegasus.Core.AzureTable;

using Microsoft.WindowsAzure.Storage;

namespace BrightSword.Pegasus.ServiceModel.CommandHandler
{
    public class CommandHandlersTable : AzureTableWrapper<CommandHandlerEntity>
    {
        public CommandHandlersTable(CloudStorageAccount cloudStorageAccount,
                                    string tableName)
            : base(cloudStorageAccount,
                   tableName) {}
    }
}