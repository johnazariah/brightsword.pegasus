﻿using System;

namespace BrightSword.Pegasus.API.Exceptions
{
    public class FailedToDownloadAssemblyException : Exception
    {
        public FailedToDownloadAssemblyException(string handlerAssembly,
                                                 Exception exception)
            : base("Failed To Download Assembly",
                   exception)
        {
            HandlerAssembly = handlerAssembly;
        }

        public string HandlerAssembly { get; set; }
    }
}