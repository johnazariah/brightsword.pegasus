﻿namespace BrightSword.Pegasus.API
{
    public interface ICommandHandler
    {
        void ProcessCommand(ICommand command,
                            ICommandHandlerContext context);
    }

    public interface ICommandHandler<TContext, in TCommand, out TCommandArgument> : ICommandHandler
        where TContext : ICommandHandlerContext
        where TCommand : ICommand
        where TCommandArgument : class, ICommandArgument, new()
    {
        TContext Context { get; }
        TCommandArgument CommandArgument { get; }

        void ProcessCommand(TCommand command,
                            TContext context);
    }
}