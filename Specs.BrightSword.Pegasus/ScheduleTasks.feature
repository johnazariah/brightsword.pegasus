﻿Feature: ScheduleTasks

Background: 
	Given A CloudRunner Storage Configuration of
		| RegisteredTasksTableName | ScheduledTasksQueueName          | RegisteredHandlersTableName | RegisteredHandlersBlobContainerName | StorageAccount | StorageAccountKey | RuntimeErrorsTableName |
		| CloudRunnerTasks         | cloudrunner-scheduled-task-queue | CloudRunnerHandlers         | cloudrunner-assemblies              | myobpdqaart    | MHDYieseAgGX6HbE8yWK4e0L2eKkUt+an6UbX1/6o0nC2FK2pmgxi/OVW+WEVyH50ROBRMtw0wxf057KDTaDfA== | CloudRunnerRuntimeErrors |

	And An empty ScheduledTasksQueue

	And An empty RegisteredTasksTable

    And A CloudRunnerTask named Sample is inserted into RegisteredTasksTable
		| PartitionKey | RowKey | ETag | Name   | TaskHandlerType | TaskInputParameters  | TaskOutputParameters | NextScheduledRuntime | CompletionStatus |
		| test_data    | 100    | *    | Sample |                 | {text:"Hello World"} |                      | 1601-01-01T00:00:00  | Pending          |

	Then Sample must have a CompletionStatus of "Pending"
	
Scenario: Access Table and Queue storage in Azure
	Then The RegisteredTasksTable should be accessible
	And The ScheduledTasksQueue should be accessible

Scenario: Should not be able to schedule a task with Queued status
	When Sample has a CompletionStatus of "Queued"
	And the TaskScheduler invokes the enqueue operation on CloudRunnerTasks
	Then a "CanOnlySchedulePendingTasks" exception should be thrown 
	And Sample must have a CompletionStatus of "Queued"

Scenario: Should not be able to schedule a task with Completed status
	When Sample has a CompletionStatus of "Completed"
	And the TaskScheduler invokes the enqueue operation on CloudRunnerTasks
	Then a "CanOnlySchedulePendingTasks" exception should be thrown 
	And Sample must have a CompletionStatus of "Completed"

Scenario: Schedule a task with Pending status with NextScheduledRuntime in the past
	When Sample has a CompletionStatus of "Pending"
	And Sample has a NextScheduledRuntime of "1601-01-01T00:00:00"
	And the TaskScheduler invokes the enqueue operation on CloudRunnerTasks
	Then A ScheduledTask must be enqueued to ScheduledCloudRunnerTasks
	And Sample must have a CompletionStatus of "Queued"
	
Scenario: Schedule a task with Pending status with NextScheduledRuntime in the past by offset
	When Sample has a CompletionStatus of "Pending"
	And Sample has a NextScheduledRuntime in the "past" by "02:00:00:00.0000000"
	And the TaskScheduler invokes the enqueue operation on CloudRunnerTasks
	Then A ScheduledTask must be enqueued to ScheduledCloudRunnerTasks
	And Sample must have a CompletionStatus of "Queued"

Scenario: Schedule a task with Pending status with NextScheduledRuntime in the future by offset
When Sample has a CompletionStatus of "Pending"
	And Sample has a NextScheduledRuntime in the "future" by "02:00:00:00.0000000"
	And the TaskScheduler invokes the enqueue operation on CloudRunnerTasks
	Then Sample must not be enqueued to ScheduledCloudRunnerTasks
	And Sample must have a CompletionStatus of "Pending"
