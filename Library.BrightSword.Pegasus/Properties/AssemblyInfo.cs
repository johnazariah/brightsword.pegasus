﻿// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("BrightSword.Pegasus")]
[assembly: AssemblyDescription("BrightSword Pegasus is a framework to build CloudScale applications")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("BrightSword Technologies Pte. Ltd., Singapore")]
[assembly: AssemblyProduct("BrightSword.Pegasus")]
[assembly: AssemblyCopyright("Copyright ©  2013, BrightSword Technologies Pte. Ltd., Singapore")]
[assembly: AssemblyTrademark("BrightSword Pegasus")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.

[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM

[assembly: Guid("18c4d30d-a5c9-4722-a418-ca6bec38fdc2")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]

[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0.*")]
[assembly: InternalsVisibleTo("Specs.BrightSword.Pegasus")]
[assembly: InternalsVisibleTo("Tests.BrightSword.Pegasus")]