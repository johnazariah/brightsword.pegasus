﻿using BrightSword.Pegasus.Configuration;

using NUnit.Framework;

using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Specs.BrightSword.Pegasus
{
    [Binding]
    public class ConfigurationSteps
    {
        [When(@"I create a configuration object from the ApplicationConfiguration settings")]
        public void WhenICreateAConfigurationObjectFromTheApplicationConfigurationSettings()
        {
            ScenarioContext.Current["ApplicationConfiguration"] = CloudRunnerConfiguration.CreateFromApplicationSettings();
        }

        [Then(@"The configuration object should be")]
        public void ThenTheConfigurationObjectShouldBe(Table table)
        {
            var expected = table.CreateInstance<CloudRunnerConfiguration>();
            var actual = (CloudRunnerConfiguration)ScenarioContext.Current["ApplicationConfiguration"];

            Assert.AreEqual(expected.CommandResultsTableName,
                            actual.CommandResultsTableName);
            Assert.AreEqual(expected.CommandQueueName,
                            actual.CommandQueueName);
            Assert.AreEqual(expected.RegisteredCommandHandlersBlobContainerName,
                            actual.RegisteredCommandHandlersBlobContainerName);
            Assert.AreEqual(expected.RegisteredCommandHandlersTableName,
                            actual.RegisteredCommandHandlersTableName);
            Assert.AreEqual(expected.StorageAccount,
                            actual.StorageAccount);
            Assert.AreEqual(expected.StorageAccountKey,
                            actual.StorageAccountKey);
        }
    }
}