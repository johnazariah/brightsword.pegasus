﻿using BrightSword.Pegasus.API;

using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;

namespace BrightSword.Pegasus.Configuration
{
    public class AzureStorageContext : IAzureStorageContext
    {
        private readonly CloudStorageAccount _cloudStorageAccount;

        protected AzureStorageContext(IAzureStorageConfiguration configuration)
        {
            StorageAccount = configuration.StorageAccount;
            StorageAccountKey = configuration.StorageAccountKey;
            _cloudStorageAccount = new CloudStorageAccount(new StorageCredentials(StorageAccount,
                                                                                  StorageAccountKey),
                                                           true);
        }

        public string StorageAccount { get; private set; }

        public string StorageAccountKey { get; private set; }

        public CloudStorageAccount CloudStorageAccount
        {
            get
            {
                return _cloudStorageAccount;
            }
        }
    }
}