﻿using System;
using System.IO;
using System.Reflection;

using BrightSword.Pegasus.API;
using BrightSword.Pegasus.Configuration;
using BrightSword.Pegasus.Entities;
using BrightSword.SwissKnife;

using ConsoleApp.Brightsword.Pegasus.RegisterTaskHandler.Properties;

using Microsoft.WindowsAzure.Storage.Blob;

namespace ConsoleApp.Brightsword.Pegasus.RegisterTaskHandler
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var parameters = new Parameters
                             {
                                 FullAssemblyPath = args.GetArgumentValue("--assembly",
                                                                          _ => _),
                                 HandlerTypeName = args.GetArgumentValue("--type",
                                                                         _ => _),
                                 HandlerName = args.GetArgumentValue("--name",
                                                                     _ => _)
                             };

            var configuration = new StorageConfiguration();

            var context = new CloudRunnerContext(configuration);
            if (String.IsNullOrWhiteSpace(parameters.HandlerName))
            {
                throw new ArgumentException(String.Format("Must specify non-empty handler name"));
            }

            var assembly = Assembly.LoadFrom(parameters.FullAssemblyPath);
            if (assembly == null)
            {
                throw new ArgumentException(String.Format("Unable to load assembly {0}",
                                                          parameters.FullAssemblyPath));
            }

            var type = assembly.GetType(parameters.HandlerTypeName);
            if (type == null)
            {
                throw new ArgumentException(String.Format("Unable to find type {0}",
                                                          parameters.HandlerTypeName));
            }

            if (!(typeof (ITaskHandler).IsAssignableFrom(type)))
            {
                throw new ArgumentException(String.Format("Type {0} is not an ITaskHandler",
                                                          parameters.HandlerTypeName));
            }

            var assemblyName = Path.GetFileNameWithoutExtension(assembly.CodeBase);

            context.RegisteredHandlersBlobContainer.SetPermissions(new BlobContainerPermissions
                                                                   {
                                                                       PublicAccess = BlobContainerPublicAccessType.Container
                                                                   });

            var blob = context.RegisteredHandlersBlobContainer.GetBlockBlobReference(assemblyName);
            if (blob.Exists())
            {
                // TODO: don't upload assembly every time...
                blob.FetchAttributes();
            }

            using (var stream = new FileStream(parameters.FullAssemblyPath,
                                               FileMode.Open,
                                               FileAccess.Read,
                                               FileShare.Read))
            {
                blob.UploadFromStream(stream);
            }

            context.RegisteredHandlersTable.EnsureInstance(new CloudRunnerTaskHandler(parameters.HandlerName,
                                                                                            type));
        }

        private class Parameters
        {
            public string FullAssemblyPath { get; set; }
            public string HandlerTypeName { get; set; }
            public string HandlerName { get; set; }
        }

        private class StorageConfiguration
        {
            public string StorageAccount
            {
                get { return Settings.Default.Pegasus_StorageAccount; }
            }

            public string StorageAccountKey
            {
                get { return Settings.Default.Pegasus_StorageAccountKey; }
            }

            public string RegisteredTasksTableName
            {
                get { return Settings.Default.Pegasus_RegisteredTasksTableName; }
            }

            public string ScheduledTasksQueueName
            {
                get { return Settings.Default.Pegasus_ScheduledTasksQueueName; }
            }

            public string RegisteredHandlersTableName
            {
                get { return Settings.Default.Pegasus_RegisteredHandlersTableName; }
            }

            public string RegisteredHandlersBlobContainerName
            {
                get { return Settings.Default.Pegasus_RegisteredHandlersBlobContainerName; }
            }

            public string RuntimeErrorsTableName
            {
                get { return Settings.Default.Pegasus_RuntimeErrorsTableName; }
            }
        }
    }
}