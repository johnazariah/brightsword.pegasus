﻿using System;

namespace BrightSword.Pegasus.API.Attributes
{
    public class RegisterCommandHandlerAttribute : Attribute
    {
        public RegisterCommandHandlerAttribute(Type type)
        {
            Type = type;
        }

        public Type Type { get; set; }
    }
}