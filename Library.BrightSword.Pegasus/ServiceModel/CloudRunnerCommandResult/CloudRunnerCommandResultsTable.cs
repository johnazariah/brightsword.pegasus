﻿using BrightSword.Pegasus.Core;
using BrightSword.Pegasus.Core.AzureTable;

using Microsoft.WindowsAzure.Storage;

namespace BrightSword.Pegasus.ServiceModel.CloudRunnerCommandResult
{
    public class CloudRunnerCommandResultsTable : AzureTableWrapper<CloudRunnerCommandResult>
    {
        public CloudRunnerCommandResultsTable(CloudStorageAccount cloudStorageAccount,
                                              string tableName)
            : base(cloudStorageAccount,
                   tableName) {}
    }
}