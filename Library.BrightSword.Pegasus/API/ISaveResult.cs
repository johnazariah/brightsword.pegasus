﻿namespace BrightSword.Pegasus.API
{
    public interface ISaveResult
    {
        string CommandId { get; }
    }
}