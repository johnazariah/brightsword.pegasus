﻿using System;

using BrightSword.Pegasus.API;

using Microsoft.WindowsAzure.Storage.Blob;

using NUnit.Framework;

using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Specs.BrightSword.Pegasus
{
    [Binding]
    public class BackgroundSteps
    {
        public AzureContext AzureContext { get; private set; }

        [Given(Regex = "A CloudRunner Storage Configuration of")]
        public void GivenACloudRunnerStorageConfigurationOf(Table table)
        {
            var storageConfiguration = table.CreateInstance<TestStorageConfiguration>();
            ScenarioContext.Current["AzureContext"] = new AzureContext(storageConfiguration);

            AzureContext = ScenarioContext.Current.Get<AzureContext>("AzureContext");
        }

        [Given(Regex = "An empty RegisteredTasksTable")]
        public void GivenAnEmptyRegisteredTasksTable()
        {
            AzureContext.RegisteredTasksTable.DeleteInstances();
        }

        [Given(Regex = "An empty ScheduledTasksQueue")]
        public void GivenAnEmptyScheduledTasksQueue()
        {
            AzureContext.ScheduledTasksQueue.DeleteAllMessages();
        }

        [Given(Regex = "An empty RegisteredHandlersTable")]
        public void GivenAnEmptyRegisteredHandlersTable()
        {
            AzureContext.RegisteredHandlersTable.DeleteInstances();
        }

        [Given(@"An empty RegisteredHandlersBlobContainer")]
        public void GivenAnEmptyRegisteredHandlersBlobContainer()
        {
            foreach (CloudBlockBlob listBlobItem in AzureContext.RegisteredHandlersBlobContainer.ListBlobs(useFlatBlobListing: true))
            {
                listBlobItem.Delete();
            }
        }

        [Given(@"The ""(.*)"" assembly is uploaded to the RegisteredHandlersBlobContainer")]
        public void GivenTheAssemblyIsUploadedToTheRegisteredHandlersBlobContainer(string assemblyName)
        {
            var blob = AzureContext.RegisteredHandlersBlobContainer.GetBlockBlobReference(assemblyName);
            Assert.IsTrue(blob.Exists());
        }

        [Given(Regex = "A TaskHandler is inserted into RegisteredHandlersTable")]
        public void GivenATaskHandlerIsInsertedIntoRegisteredHandlersTable(Table table)
        {
            var insertedInstance = AzureContext.RegisteredHandlersTable.EnsureInstance(table.CreateInstance<CloudRunnerTaskHandler>(),
                                                                                       true);
            insertedInstance.VerifyInsertedInstance(table);
        }

        [Given(Regex = "A CloudRunnerTask named Sample is inserted into RegisteredTasksTable")]
        [Given(Regex = "A CloudRunnerTask is inserted into RegisteredTasksTable")]
        public void GivenACloudRunnerTaskIsInsertedIntoRegisteredTasksTable(Table table)
        {
            AzureContext.Sample = AzureContext.RegisteredTasksTable.EnsureInstance(ReadCloudRunnerTaskFromTable(table),
                                                                                   true);

            AzureContext.Sample.VerifyInsertedInstance(table,
                                                       ReadCloudRunnerTaskFromTable,
                                                       CompareInstances);
        }

        [Then(Regex = "Insert Some CloudRunnerTasks into RegisteredTasksTable")]
        public void ThenInsertSomeCloudRunnerTasksIntoRegisteredTasksTable(Table table)
        {
            var tasks = table.CreateSet<CloudRunnerTask>();

            foreach (var task in tasks)
            {
                var actual = AzureContext.RegisteredTasksTable.EnsureInstance(task);

                // this is only set on the server side...
                // ... so ensure it is set...
                Assert.AreNotEqual(DateTime.MinValue,
                                   actual.Timestamp);
                // ... and that it doesn't cause a false failure
                task.Timestamp = actual.Timestamp;

                Assert.IsTrue(actual.AllPropertiesAreEqualWith(task));
            }
        }

        private static CloudRunnerTask ReadCloudRunnerTaskFromTable(Table table)
        {
            var sample = table.CreateInstance<CloudRunnerTask>();

            CompletionStatus completionStatus;
            var parseSuccessful = Enum.TryParse(table.Rows[0]["CompletionStatus"],
                                                out completionStatus);

            Assert.IsTrue(parseSuccessful);

            sample.CompletionStatus = completionStatus.ToString();
            return sample;
        }

        private static void CompareInstances(CloudRunnerTask expected,
                                             CloudRunnerTask actual)
        {
            Assert.AreEqual(expected.RowKey,
                            actual.RowKey);

            Assert.AreEqual(expected.PartitionKey,
                            actual.PartitionKey);

            Assert.AreEqual(expected.Name,
                            actual.Name);

            Assert.AreEqual(expected.TaskHandlerName,
                            actual.TaskHandlerName);

            Assert.AreEqual(expected.TaskInputParameters,
                            actual.TaskInputParameters);

            Assert.AreEqual(expected.TaskOutputParameters,
                            actual.TaskOutputParameters);

            Assert.AreEqual(expected.NextScheduledRuntime,
                            actual.NextScheduledRuntime);

            Assert.AreEqual(expected.CompletionStatus,
                            actual.CompletionStatus);
        }

        [Then(@"Sample must have a CompletionStatus of ""(.*)""")]
        public void GivenSampleMustHaveACompletionStatusOf(string statusName)
        {
            var verifiedSampleRow = AzureContext.RegisteredTasksTable.RetrieveInstance(AzureContext.Sample);

            CompletionStatus status;
            if (!(Enum.TryParse(statusName,
                                out status)))
            {
                Assert.Fail("{0} is not a valid CompletionStatus enumeration value!",
                            statusName);
            }

            Assert.AreEqual(statusName,
                            verifiedSampleRow.CompletionStatus);
        }
    }
}