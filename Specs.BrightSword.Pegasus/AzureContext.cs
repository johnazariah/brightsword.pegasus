﻿using BrightSword.Pegasus.API;
using BrightSword.Pegasus.Configuration;

namespace Specs.BrightSword.Pegasus
{
    public class AzureContext : CloudRunnerContext
    {
        public AzureContext(ICloudRunnerConfiguration storageConfiguration)
            : base(storageConfiguration) {}

        public CloudRunnerTask Sample { get; set; }
    }
}