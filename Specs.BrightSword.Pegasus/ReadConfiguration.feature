﻿Feature: ReadConfiguration
	I want to specify a configuration file with parameters
	So that the Pegasus library can be configured appropriately

Scenario: Load Configuration From Application Configuration
	When I create a configuration object from the ApplicationConfiguration settings
	Then The configuration object should be
	| StorageAccount | StorageAccountKey                                                                        | RegisteredCommandHandlersTableName | RegisteredCommandHandlersBlobContainerName | CommandQueueName             | CommandResultsTableName      |
	| whingepool     | dzYvfFwlBjuZBMp6ahhodxvjJa3R00KL7sos9YHsZatVVr/z71Dr18ZqdEVhSkJJQz5ZGZLoaBK1O6C+kzQZ7Q== | WhingePoolTestCommandHandlers      | whingepool-test-command-handler-assemblies | whingepool-test-whinge-queue | WhingePoolTestCommandResults |
	
