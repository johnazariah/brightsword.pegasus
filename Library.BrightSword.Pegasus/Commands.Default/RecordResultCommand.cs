﻿using BrightSword.Pegasus.Commands.Core;

using Microsoft.WindowsAzure.Storage.Queue;

namespace BrightSword.Pegasus.Commands.Default
{
    public class RecordResultCommand : Command<RecordResultCommandArgument>
    {
        public RecordResultCommand(RecordResultCommandArgument argument)
            : base(argument) {}

        public RecordResultCommand(CloudQueueMessage message)
            : base(message) {}
    }
}