﻿using System;
using System.Globalization;

using BrightSword.Pegasus.API;

using Microsoft.WindowsAzure.Storage.Queue;

using Newtonsoft.Json;

namespace BrightSword.Pegasus.Commands.Core
{
    public class Command : ICommand
    {
        public Command() {}

        protected Command(string commandArgumentTypeName,
                          string serializedCommandArgument,
                          string commandName = null)
        {
            CommandId = DateTime.UtcNow.Ticks.ToString(CultureInfo.InvariantCulture);
            CommandName = commandName ?? GetType()
                                             .FullName;
            CommandArgumentTypeName = commandArgumentTypeName;
            SerializedCommandArgument = serializedCommandArgument;
        }

        public string CommandId { get; set; }

        public string CommandName { get; set; }

        public string CommandArgumentTypeName { get; set; }

        public string SerializedCommandArgument { get; set; }

        public static explicit operator CloudQueueMessage(Command _this)
        {
            return new CloudQueueMessage(JsonConvert.SerializeObject(_this));
        }

        public static explicit operator Command(CloudQueueMessage _this)
        {
            return _this == null
                       ? null
                       : JsonConvert.DeserializeObject<Command>(_this.AsString);
        }
    }

    public abstract class Command<TCommandArgument> : Command,
                                                      ICommand<TCommandArgument>
        where TCommandArgument : class, ICommandArgument, new()
    {
        protected Command(TCommandArgument argument)
            : base(typeof (TCommandArgument).FullName,
                   JsonConvert.SerializeObject(argument))
        {
            CommandArgument = argument;
        }

        protected Command(CloudQueueMessage message)
            : base(typeof (TCommandArgument).FullName,
                   JsonConvert.SerializeObject(message.AsString))
        {
            CommandArgument = JsonConvert.DeserializeObject<TCommandArgument>(message.AsString);
        }

        public TCommandArgument CommandArgument { get; private set; }

        public static TCommand CreateFromCloudQueueMessage<TCommand, TArgument>(CloudQueueMessage message)
            where TCommand : Command<TArgument>, new()
            where TArgument : class, ICommandArgument, new()
        {
            var command = new TCommand
                          {
                              CommandArgument = JsonConvert.DeserializeObject<TArgument>(message.AsString)
                          };

            return command;
        }

        public static explicit operator CloudQueueMessage(Command<TCommandArgument> _this)
        {
            return new CloudQueueMessage(JsonConvert.SerializeObject(_this));
        }
    }
}