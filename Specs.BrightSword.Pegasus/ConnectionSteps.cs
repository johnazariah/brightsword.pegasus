﻿using NUnit.Framework;

using TechTalk.SpecFlow;

namespace Specs.BrightSword.Pegasus
{
    [Binding]
    public class ConnectionSteps
    {
        public ConnectionSteps()
        {
            AzureContext = ScenarioContext.Current.Get<AzureContext>("AzureContext");
            Assert.IsNotNull(AzureContext);
        }

        public AzureContext AzureContext { get; private set; }

        [Then(@"The RegisteredTasksTable should be accessible")]
        public void ThenTheRegisteredTasksTableShouldBeAccessible()
        {
            Assert.IsNotNull(AzureContext);
            Assert.IsNotNull(AzureContext.RegisteredTasksTable);
            Assert.IsTrue(AzureContext.RegisteredTasksTable.Table.Exists());
        }

        [Then(@"The ScheduledTasksQueue should be accessible")]
        public void ThenTheScheduledTasksQueueShouldBeAccessible()
        {
            Assert.IsNotNull(AzureContext);
            Assert.IsNotNull(AzureContext.ScheduledTasksQueue);
            Assert.IsTrue(AzureContext.ScheduledTasksQueue.Exists());
        }
    }
}