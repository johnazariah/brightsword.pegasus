﻿Feature: AddTask

Scenario: Add Some Tasks 
	Given A CloudRunner Storage Configuration of
		| RegisteredTasksTableName | ScheduledTasksQueueName          | RegisteredHandlersTableName | RegisteredHandlersBlobContainerName | StorageAccount | StorageAccountKey | RuntimeErrorsTableName |
		| CloudRunnerTasks         | cloudrunner-scheduled-task-queue | CloudRunnerHandlers         | cloudrunner-assemblies              | myobpdqaart    | MHDYieseAgGX6HbE8yWK4e0L2eKkUt+an6UbX1/6o0nC2FK2pmgxi/OVW+WEVyH50ROBRMtw0wxf057KDTaDfA== | CloudRunnerRuntimeErrors |
	
	And An empty RegisteredTasksTable

	Then Insert Some CloudRunnerTasks into RegisteredTasksTable
		| PartitionKey | RowKey | ETag | Name   | TaskHandlerName      | TaskInputParameters                                                                                         | TaskOutputParameters | NextScheduledRuntime | CompletionStatus |
		| test_data    | 100    | *    | Sample | DistributedQueryTask | {name: "NumberOfCards", query:"select db_name(), count(RecordId) from Card"}                                |                      | 1601-01-01T00:00:00  | Pending          |
		| test_data    | 200    | *    | Sample | DistributedQueryTask | {name: "NumberOfEmployees", query:"select db_name(), select count(RecordID) from Card where CardType = 17"} |                      | 1601-01-01T00:00:00  | Pending          |










