﻿using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage.Table.Protocol;

namespace Tests.BrightSword.Pegasus
{
    [TestClass]
    public class DateTimeParseTests
    {
        [TestMethod]
        public void ParseExactForSortableFormat()
        {
            var dateTime = DateTime.ParseExact(
                "2013-06-13T08:02:07",
                "s",
                null);

            Assert.AreEqual(
                new DateTime(
                    2013,
                    06,
                    13,
                    08,
                    02,
                    07),
                dateTime);
        }

        [TestMethod]
        public void GetStringForMinDatetime()
        {
            Assert.AreEqual(
                "1601-01-01T00:00:00",
                TableConstants.MinDateTime.ToString("s"));

            Assert.AreEqual(
                TableConstants.MinDateTime,
                DateTime.ParseExact(
                    "1601-01-01T00:00:00",
                    "s",
                    null)
                        .ToLocalTime());
        }
    }
}