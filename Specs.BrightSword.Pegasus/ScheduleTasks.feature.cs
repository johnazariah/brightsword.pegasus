﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:1.9.0.77
//      SpecFlow Generator Version:1.9.0.0
//      Runtime Version:4.0.30319.18047
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace Specs.BrightSword.Pegasus
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "1.9.0.77")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("ScheduleTasks")]
    public partial class ScheduleTasksFeature
    {
        
        private static TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "ScheduleTasks.feature"
#line hidden
        
        [NUnit.Framework.TestFixtureSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "ScheduleTasks", "", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.TestFixtureTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void FeatureBackground()
        {
#line 3
#line hidden
            TechTalk.SpecFlow.Table table1 = new TechTalk.SpecFlow.Table(new string[] {
                        "RegisteredTasksTableName",
                        "ScheduledTasksQueueName",
                        "RegisteredHandlersTableName",
                        "RegisteredHandlersBlobContainerName",
                        "StorageAccount",
                        "StorageAccountKey",
                        "RuntimeErrorsTableName"});
            table1.AddRow(new string[] {
                        "CloudRunnerTasks",
                        "cloudrunner-scheduled-task-queue",
                        "CloudRunnerHandlers",
                        "cloudrunner-assemblies",
                        "myobpdqaart",
                        "MHDYieseAgGX6HbE8yWK4e0L2eKkUt+an6UbX1/6o0nC2FK2pmgxi/OVW+WEVyH50ROBRMtw0wxf057KD" +
                            "TaDfA==",
                        "CloudRunnerRuntimeErrors"});
#line 4
 testRunner.Given("A CloudRunner Storage Configuration of", ((string)(null)), table1, "Given ");
#line 8
 testRunner.And("An empty ScheduledTasksQueue", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 10
 testRunner.And("An empty RegisteredTasksTable", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table2 = new TechTalk.SpecFlow.Table(new string[] {
                        "PartitionKey",
                        "RowKey",
                        "ETag",
                        "Name",
                        "TaskHandlerType",
                        "TaskInputParameters",
                        "TaskOutputParameters",
                        "NextScheduledRuntime",
                        "CompletionStatus"});
            table2.AddRow(new string[] {
                        "test_data",
                        "100",
                        "*",
                        "Sample",
                        "",
                        "{text:\"Hello World\"}",
                        "",
                        "1601-01-01T00:00:00",
                        "Pending"});
#line 12
    testRunner.And("A CloudRunnerTask named Sample is inserted into RegisteredTasksTable", ((string)(null)), table2, "And ");
#line 16
 testRunner.Then("Sample must have a CompletionStatus of \"Pending\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Access Table and Queue storage in Azure")]
        public virtual void AccessTableAndQueueStorageInAzure()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Access Table and Queue storage in Azure", ((string[])(null)));
#line 18
this.ScenarioSetup(scenarioInfo);
#line 3
this.FeatureBackground();
#line 19
 testRunner.Then("The RegisteredTasksTable should be accessible", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 20
 testRunner.And("The ScheduledTasksQueue should be accessible", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Should not be able to schedule a task with Queued status")]
        public virtual void ShouldNotBeAbleToScheduleATaskWithQueuedStatus()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Should not be able to schedule a task with Queued status", ((string[])(null)));
#line 22
this.ScenarioSetup(scenarioInfo);
#line 3
this.FeatureBackground();
#line 23
 testRunner.When("Sample has a CompletionStatus of \"Queued\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 24
 testRunner.And("the TaskScheduler invokes the enqueue operation on CloudRunnerTasks", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 25
 testRunner.Then("a \"CanOnlySchedulePendingTasks\" exception should be thrown", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 26
 testRunner.And("Sample must have a CompletionStatus of \"Queued\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Should not be able to schedule a task with Completed status")]
        public virtual void ShouldNotBeAbleToScheduleATaskWithCompletedStatus()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Should not be able to schedule a task with Completed status", ((string[])(null)));
#line 28
this.ScenarioSetup(scenarioInfo);
#line 3
this.FeatureBackground();
#line 29
 testRunner.When("Sample has a CompletionStatus of \"Completed\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 30
 testRunner.And("the TaskScheduler invokes the enqueue operation on CloudRunnerTasks", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 31
 testRunner.Then("a \"CanOnlySchedulePendingTasks\" exception should be thrown", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 32
 testRunner.And("Sample must have a CompletionStatus of \"Completed\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Schedule a task with Pending status with NextScheduledRuntime in the past")]
        public virtual void ScheduleATaskWithPendingStatusWithNextScheduledRuntimeInThePast()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Schedule a task with Pending status with NextScheduledRuntime in the past", ((string[])(null)));
#line 34
this.ScenarioSetup(scenarioInfo);
#line 3
this.FeatureBackground();
#line 35
 testRunner.When("Sample has a CompletionStatus of \"Pending\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 36
 testRunner.And("Sample has a NextScheduledRuntime of \"1601-01-01T00:00:00\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 37
 testRunner.And("the TaskScheduler invokes the enqueue operation on CloudRunnerTasks", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 38
 testRunner.Then("A ScheduledTask must be enqueued to ScheduledCloudRunnerTasks", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 39
 testRunner.And("Sample must have a CompletionStatus of \"Queued\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Schedule a task with Pending status with NextScheduledRuntime in the past by offs" +
            "et")]
        public virtual void ScheduleATaskWithPendingStatusWithNextScheduledRuntimeInThePastByOffset()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Schedule a task with Pending status with NextScheduledRuntime in the past by offs" +
                    "et", ((string[])(null)));
#line 41
this.ScenarioSetup(scenarioInfo);
#line 3
this.FeatureBackground();
#line 42
 testRunner.When("Sample has a CompletionStatus of \"Pending\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 43
 testRunner.And("Sample has a NextScheduledRuntime in the \"past\" by \"02:00:00:00.0000000\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 44
 testRunner.And("the TaskScheduler invokes the enqueue operation on CloudRunnerTasks", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 45
 testRunner.Then("A ScheduledTask must be enqueued to ScheduledCloudRunnerTasks", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 46
 testRunner.And("Sample must have a CompletionStatus of \"Queued\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Schedule a task with Pending status with NextScheduledRuntime in the future by of" +
            "fset")]
        public virtual void ScheduleATaskWithPendingStatusWithNextScheduledRuntimeInTheFutureByOffset()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Schedule a task with Pending status with NextScheduledRuntime in the future by of" +
                    "fset", ((string[])(null)));
#line 48
this.ScenarioSetup(scenarioInfo);
#line 3
this.FeatureBackground();
#line 49
testRunner.When("Sample has a CompletionStatus of \"Pending\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 50
 testRunner.And("Sample has a NextScheduledRuntime in the \"future\" by \"02:00:00:00.0000000\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 51
 testRunner.And("the TaskScheduler invokes the enqueue operation on CloudRunnerTasks", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 52
 testRunner.Then("Sample must not be enqueued to ScheduledCloudRunnerTasks", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 53
 testRunner.And("Sample must have a CompletionStatus of \"Pending\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
