﻿using System;
using System.Collections.Generic;
using System.Linq;

using BrightSword.Pegasus.API;

using Microsoft.WindowsAzure.Storage.Table;

using Newtonsoft.Json;

namespace BrightSword.Pegasus.Commands.Default
{
    public class RecordResultCommandArgument : ICommandArgument
    {
        public RecordResultCommandArgument() {}

        public RecordResultCommandArgument(string jsonEntity)
        {
            var map = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonEntity);

            ResultTableName = map["ResultTableName"];
            PartitionKey = map["PartitionKey"];
            RowKey = map["RowKey"];
            Properties = new Dictionary<string, EntityProperty>();

            foreach (var kvp in map)
            {
                switch (kvp.Key)
                {
                    case "ResultTableName":
                    case "PartitionKey":
                    case "RowKey":
                        break;
                    default:
                        Properties.Add(kvp.Key,
                                       new EntityProperty(kvp.Value));
                        break;
                }
            }
        }

        public RecordResultCommandArgument(string resultTableName,
                                           string partitionKey,
                                           string rowKey,
                                           Dictionary<string, EntityProperty> properties)
        {
            AllProperties = Properties = properties;
            AllProperties["ResultTableName"] = new EntityProperty(ResultTableName = resultTableName);
            AllProperties["PartitionKey"] = new EntityProperty(PartitionKey = partitionKey);
            AllProperties["RowKey"] = new EntityProperty(RowKey = rowKey);
        }

        public string ResultTableName { get; set; }

        public string PartitionKey { get; set; }

        public string RowKey { get; set; }

        public Dictionary<string, EntityProperty> Properties { get; set; }

        public Dictionary<string, EntityProperty> AllProperties { get; set; }

        public DynamicTableEntity Entity
        {
            get
            {
                return new DynamicTableEntity(PartitionKey,
                                              RowKey)
                       {
                           Properties = Properties
                       };
            }
        }

        public string ToJsonString()
        {
            var entries = AllProperties.Select(_ => String.Format("\"{0}\":\"{1}\"",
                                                                  _.Key,
                                                                  String.Join(",",
                                                                              _.Value.StringValue)));
            return String.Format("{{{0}}}",
                                 String.Join(",",
                                             entries));
        }
    }
}