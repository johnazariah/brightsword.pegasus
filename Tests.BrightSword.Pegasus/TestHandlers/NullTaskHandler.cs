﻿using BrightSword.Pegasus.Configuration;

namespace Test.Handlers
{
    public class NullTaskHandler : ITaskHandler
    {
        public void ProcessTask(CloudRunnerContext context,
                                ICloudRunnerTask task) {}
    }
}