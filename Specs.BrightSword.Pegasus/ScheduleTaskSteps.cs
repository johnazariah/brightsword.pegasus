﻿using System;
using System.Globalization;

using BrightSword.Pegasus.API;

using Newtonsoft.Json;

using NUnit.Framework;

using TechTalk.SpecFlow;

namespace Specs.BrightSword.Pegasus
{
    [Binding]
    public class ScheduleTaskSteps
    {
        private string _thrownExceptionTypeName;

        public ScheduleTaskSteps()
        {
            AzureContext = ScenarioContext.Current.Get<AzureContext>("AzureContext");
            Assert.IsNotNull(AzureContext);
        }

        private AzureContext AzureContext { get; set; }

        [When(@"Sample has a CompletionStatus of ""(.*)""")]
        public void WhenSampleHasACompletionStatusOf(string statusString)
        {
            CompletionStatus status;
            var parseResult = Enum.TryParse(statusString,
                                            out status);

            Assert.IsTrue(parseResult);

            if (AzureContext.Sample.CompletionStatus == status.ToString())
            {
                return;
            }

            AzureContext.Sample.CompletionStatus = status.ToString();
            AzureContext.RegisteredTasksTable.EnsureInstance(AzureContext.Sample);
        }

        [When(@"Sample has a NextScheduledRuntime of ""(.*)""")]
        public void WhenSampleHasANextScheduledRuntimeOf(string nextScheduledRunTimeString)
        {
            DateTime nextScheduledRunTime;

            var parseResult = DateTime.TryParseExact(nextScheduledRunTimeString,
                                                     "s",
                                                     null,
                                                     DateTimeStyles.AssumeUniversal,
                                                     out nextScheduledRunTime);

            Assert.IsTrue(parseResult);

            AzureContext.Sample.NextScheduledRuntime = nextScheduledRunTime;
            AzureContext.RegisteredTasksTable.EnsureInstance(AzureContext.Sample);
        }

        [When(@"Sample has a NextScheduledRuntime in the ""(.*)"" by ""(.*)""")]
        public void WhenSampleHasANextScheduledRuntimeInTheBy(string futureOrPast,
                                                              string timeOffsetString)
        {
            TimeSpan timeOffset;

            var parseResult = TimeSpan.TryParseExact(timeOffsetString,
                                                     "G",
                                                     null,
                                                     TimeSpanStyles.None,
                                                     out timeOffset);

            Assert.IsTrue(parseResult);

            var nextScheduledRunTime = futureOrPast.Equals("future",
                                                           StringComparison.InvariantCultureIgnoreCase)
                                           ? DateTime.Now.Add(timeOffset)
                                           : DateTime.Now.Subtract(timeOffset);

            AzureContext.Sample.NextScheduledRuntime = nextScheduledRunTime;
            AzureContext.RegisteredTasksTable.EnsureInstance(AzureContext.Sample);
        }

        [When(@"the TaskScheduler invokes the enqueue operation on CloudRunnerTasks")]
        public void WhenTheTaskSchedulerInvokesTheEnqueueOperationOnCloudRunnerTasks()
        {
            try
            {
                AzureContext.Sample = RoleBase.ScheduleTask(AzureContext.Sample,
                                                            AzureContext);
            }
            catch (CanOnlySchedulePendingTasks exception)
            {
                _thrownExceptionTypeName = exception.GetType()
                                                    .Name;
            }
        }

        [Then(@"A ScheduledTask must be enqueued to ScheduledCloudRunnerTasks")]
        public void ThenSampleMustBeEnqueuedToScheduledCloudRunnerTasks()
        {
            var message = AzureContext.ScheduledTasksQueue.PeekMessage();

            var actual = JsonConvert.DeserializeObject<CloudRunnerTask>(message.AsString);
            Assert.AreEqual(AzureContext.Sample.PartitionKey,
                            actual.PartitionKey);
            Assert.AreEqual(AzureContext.Sample.RowKey,
                            actual.RowKey);
        }

        [Then(@"Sample must not be enqueued to ScheduledCloudRunnerTasks")]
        public void ThenSampleMustBeNotToEnqueuedToCloudRunnerTasks()
        {
            var message = AzureContext.ScheduledTasksQueue.PeekMessage();
            Assert.IsNull(message);
        }

        [Then(@"a ""(.*)"" exception should be thrown")]
        public void ThenAExceptionShouldBeThrown(string exceptionTypeName)
        {
            Assert.AreEqual(exceptionTypeName,
                            _thrownExceptionTypeName);
        }
    }
}