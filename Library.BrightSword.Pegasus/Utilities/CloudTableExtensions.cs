﻿using System.Collections.Generic;

using BrightSword.SwissKnife;

using Microsoft.WindowsAzure.Storage.Table;

namespace BrightSword.Pegasus.Utilities
{
    public static class CloudTableExtensions
    {
        public static IEnumerable<DynamicTableEntity> GetInstancesInPartition(this CloudTable _this,
                                                                              string partitionKey)
        {
            return _this.GetInstancesInPartition<DynamicTableEntity>(partitionKey);
        }

        public static IEnumerable<T> GetInstancesInPartition<T>(this CloudTable _this,
                                                                string partitionKey) where T : ITableEntity, new()
        {
            var filterCondition = TableQuery.GenerateFilterCondition((ObjectDescriber.GetName((T _) => _.PartitionKey)),
                                                                     QueryComparisons.Equal,
                                                                     partitionKey);
            var filter = new TableQuery<T>().Where(filterCondition);

            return _this.ExecuteQuery(filter);
        }
    }
}