﻿using System.Collections.Generic;

using BrightSword.Pegasus.Utilities;

using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace BrightSword.Pegasus.Core.AzureTable
{
    public abstract class AzureTableWrapper<T>
        where T : class, ITableEntity, new()
    {
        protected AzureTableWrapper(CloudStorageAccount cloudStorageAccount,
                                    string tableName)
        {
            CloudStorageAccount = cloudStorageAccount;

            CloudTableClient = new CloudTableClient(CloudStorageAccount.TableEndpoint,
                                                    CloudStorageAccount.Credentials);

            TableName = tableName;

            Table = CloudTableClient.GetTableReference(TableName);
            Table.CreateIfNotExists();
        }

        public CloudStorageAccount CloudStorageAccount { get; private set; }

        public CloudTableClient CloudTableClient { get; private set; }

        public string TableName { get; private set; }

        public CloudTable Table { get; private set; }

        public virtual T EnsureInstance(T instance,
                                        bool replaceIfExists = false)
        {
            Table.Execute(replaceIfExists
                              ? TableOperation.InsertOrReplace(instance)
                              : TableOperation.InsertOrMerge(instance));

            return RetrieveInstance(instance);
        }

        public virtual T RetrieveInstance(T instance)
        {
            var retrievedRow = Table.Execute(TableOperation.Retrieve<T>(instance.PartitionKey,
                                                                        instance.RowKey));

            return (T) retrievedRow.Result;
        }

        public virtual T RetrieveInstanceByKeys(string partitionKey,
                                                string rowKey)
        {
            var retrievedRow = Table.Execute(TableOperation.Retrieve<T>(partitionKey,
                                                                        rowKey));

            return (T) retrievedRow.Result;
        }

        public virtual T RetrieveInstanceByRowKey(string rowKey)
        {
            var newInstance = new T
                              {
                                  RowKey = rowKey
                              };

            var retrievedRow = Table.Execute(TableOperation.Retrieve<T>(newInstance.PartitionKey,
                                                                        newInstance.RowKey));

            return (T) retrievedRow.Result;
        }

        public virtual void DeleteInstances(TableQuery<T> filter = null)
        {
            foreach (var instance in Table.ExecuteQuery(filter ?? new TableQuery<T>()))
            {
                DeleteInstance(instance);
            }
        }

        public virtual void DeleteInstance(T instance)
        {
            var existingInstance = RetrieveInstance(instance);

            if (existingInstance != null)
            {
                if (string.IsNullOrWhiteSpace(existingInstance.ETag))
                {
                    existingInstance.ETag = "*";
                }

                Table.Execute(TableOperation.Delete(existingInstance));
            }
        }

        public virtual IEnumerable<T> GetInstances(TableQuery<T> query = null)
        {
            return Table.ExecuteQuery(query ?? new TableQuery<T>());
        }

        public virtual IEnumerable<T> GetInstancesInPartition(string partitionKey)
        {
            return Table.GetInstancesInPartition<T>(partitionKey);
        }
    }
}