﻿Feature: ProcessTask

Background: 
	Given A CloudRunner Storage Configuration of
		| RegisteredTasksTableName | ScheduledTasksQueueName          | RegisteredHandlersTableName | RegisteredHandlersBlobContainerName | StorageAccount | StorageAccountKey | RuntimeErrorsTableName |
		| CloudRunnerTasks         | cloudrunner-scheduled-task-queue | CloudRunnerHandlers         | cloudrunner-assemblies              | myobpdqaart    | MHDYieseAgGX6HbE8yWK4e0L2eKkUt+an6UbX1/6o0nC2FK2pmgxi/OVW+WEVyH50ROBRMtw0wxf057KDTaDfA== | CloudRunnerRuntimeErrors |

	And An empty ScheduledTasksQueue

	#And An empty RegisteredHandlersTable

	And A TaskHandler is inserted into RegisteredHandlersTable
		| HandlerName | HandlerTypeAssemblyQualifiedName           | HandlerAssembly     |
		| Null        | BrightSword.Pegasus.Tasks.NullTask.Handler | BrightSword.Pegasus |

Scenario: Process a General Scheduled task (Happy Case)
	Given An empty RegisteredTasksTable

	Given A CloudRunnerTask is inserted into RegisteredTasksTable
		| PartitionKey | RowKey | ETag | Name   | TaskHandlerName | TaskInputParameters  | TaskOutputParameters | NextScheduledRuntime | CompletionStatus |
		| test_data    | 100    | *    | Sample | Null            | {text:"Hello World"} |                      | 1601-01-01T00:00:00  | Pending          |

	When Sample has a NextScheduledRuntime in the "past" by "02:00:00:00.0000000"
	And the TaskScheduler invokes the enqueue operation on CloudRunnerTasks
	Then A ScheduledTask must be enqueued to ScheduledCloudRunnerTasks
	And Sample must have a CompletionStatus of "Queued"

	When A ScheduledTask is dequeued from ScheduledCloudRunnerTasks
	And the TaskProcess invokes the process operation on the ScheduledTask
	Then The ScheduledTask should not exist in ScheduledCloudRunnerTasks any more
	And Sample must have a CompletionStatus of "Completed"

Scenario: Process a task with an unspecified TaskHandler
	Given An empty RegisteredTasksTable

	And A CloudRunnerTask is inserted into RegisteredTasksTable
		| PartitionKey | RowKey | ETag | Name   | TaskHandlerName | TaskInputParameters  | TaskOutputParameters | NextScheduledRuntime | CompletionStatus |
		| test_data    | 100    | *    | Sample |                 | {text:"Hello World"} |                      | 1601-01-01T00:00:00  | Pending          |

	When Sample has a NextScheduledRuntime in the "past" by "02:00:00:00.0000000"
	And the TaskScheduler invokes the enqueue operation on CloudRunnerTasks
	Then A ScheduledTask must be enqueued to ScheduledCloudRunnerTasks
	And Sample must have a CompletionStatus of "Queued"

	When A ScheduledTask is dequeued from ScheduledCloudRunnerTasks
	And the TaskProcess invokes the process operation on the ScheduledTask
	Then The ScheduledTask should not exist in ScheduledCloudRunnerTasks any more
	And Sample must have a CompletionStatus of "Warning_NoHandlerSpecified"

Scenario: Process a task with an unknown TaskHandler
	Given An empty RegisteredTasksTable

	And A CloudRunnerTask is inserted into RegisteredTasksTable
		| PartitionKey | RowKey | ETag | Name   | TaskHandlerName | TaskInputParameters  | TaskOutputParameters | NextScheduledRuntime | CompletionStatus |
		| test_data    | 100    | *    | Sample | Nonexistent     | {text:"Hello World"} |                      | 1601-01-01T00:00:00  | Pending          |

	When Sample has a NextScheduledRuntime in the "past" by "02:00:00:00.0000000"
	And the TaskScheduler invokes the enqueue operation on CloudRunnerTasks
	Then A ScheduledTask must be enqueued to ScheduledCloudRunnerTasks
	And Sample must have a CompletionStatus of "Queued"

	When A ScheduledTask is dequeued from ScheduledCloudRunnerTasks
	And the TaskProcess invokes the process operation on the ScheduledTask
	Then The ScheduledTask should not exist in ScheduledCloudRunnerTasks any more
	And Sample must have a CompletionStatus of "Error_HandlerCouldNotBeLoaded"
	