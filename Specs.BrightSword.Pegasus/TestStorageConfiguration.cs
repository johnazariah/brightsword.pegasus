﻿using BrightSword.Pegasus.API;

namespace Specs.BrightSword.Pegasus
{
    internal class TestStorageConfiguration : IStorageConfiguration
    {
        public string StorageAccount { get; set; }

        public string StorageAccountKey { get; set; }

        public string RegisteredTasksTableName { get; set; }

        public string ScheduledTasksQueueName { get; set; }

        public string RegisteredHandlersTableName { get; set; }

        public string RegisteredHandlersBlobContainerName { get; set; }

        public string RuntimeErrorsTableName { get; set; }
    }
}