﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

using BrightSword.Pegasus.API;
using BrightSword.Pegasus.API.Attributes;
using BrightSword.Pegasus.API.Exceptions;
using BrightSword.SwissKnife;

using Microsoft.WindowsAzure.Storage.Table;

namespace BrightSword.Pegasus.ServiceModel.CommandHandler
{
    public static class CommandHandlerService
    {
        public static ICommandHandler GetCommandHandler(this ICommand command,
                                                        ICommandHandlerContext context)
        {
            return context.GetOrAdd(command.CommandName,
                                    () => GetCommandHandlerInternal(command,
                                                                    context));
        }

        private static ICommandHandler GetCommandHandlerInternal(this ICommand command,
                                                                 ICommandHandlerContext context)
        {
            Exception thrownException;
            try
            {
                return GetDynamicCommandHandler(command,
                                                context);
            }
            catch (Exception exception)
            {
                thrownException = exception;
            }

            try
            {
                return GetDefaultCommandHandler(command);
            }
            catch (Exception)
            {
                throw thrownException;
            }
        }

        private static ICommandHandler GetDefaultCommandHandler(ICommand command)
        {
            var commandHandlers = from assembly in AppDomain.CurrentDomain.GetAssemblies()
                                  from commandHandlerType in assembly.GetCommandHandlerTypes()
                                  where commandHandlerType.CommandName.Equals(command.CommandName,
                                                                              StringComparison.InvariantCulture)
                                  select commandHandlerType.Type.Instantiate<ICommandHandler>();

            return commandHandlers.FirstOrDefault();
        }

        public static IEnumerable<CommandHandlerType> GetCommandHandlerTypes(this Assembly assembly)
        {
            return from type in assembly.ExportedTypes
                   where typeof (ICommandHandler).IsAssignableFrom(type)
                   let attr = type.GetCustomAttribute<RegisterCommandHandlerAttribute>()
                   where attr != null
                   select new CommandHandlerType
                          {
                              Type = type,
                              Attribute = attr
                          };
        }

        private static ICommandHandler GetDynamicCommandHandler(ICommand command,
                                                                ICommandHandlerContext context)
        {
            var filter = TableQuery.GenerateFilterCondition(ObjectDescriber.GetName((CommandHandlerEntity _) => _.CommandName),
                                                            QueryComparisons.Equal,
                                                            command.CommandName);

            var query = new TableQuery<CommandHandlerEntity>().Where(filter);

            var commandHandlerEntity = context.RegisteredCommandHandlersTable.GetInstances(query)
                                              .FirstOrDefault();

            if (commandHandlerEntity == null)
            {
                throw new NoRegisteredHandlerFoundException(command.CommandName);
            }

            var assembly = LoadAssembly(commandHandlerEntity) ?? DownloadAndLoadAssembly(commandHandlerEntity,
                                                                                         context);

            if (assembly == null)
            {
                throw new AssemblyFailedToLoadException(commandHandlerEntity.CommandHandlerTypeAssembly);
            }

            return assembly.GetCommandHandler(commandHandlerEntity);
        }

        private static Assembly LoadAssembly(this CommandHandlerEntity commandHandlerEntity)
        {
            try
            {
                return Assembly.Load(commandHandlerEntity.CommandHandlerTypeAssembly);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static Assembly DownloadAndLoadAssembly(this CommandHandlerEntity commandHandlerEntity,
                                                        ICommandHandlerContext context)
        {
            MemoryStream stream;
            try
            {
                stream = context.GetOrAdd(commandHandlerEntity.CommandHandlerTypeAssembly,
                                          () => commandHandlerEntity.DownloadAssemblyFromBlob(context));
            }
            catch (Exception exception)
            {
                throw new FailedToDownloadAssemblyException(commandHandlerEntity.CommandHandlerTypeAssembly,
                                                            exception);
            }

            try
            {
                return Assembly.Load(stream.GetBuffer());
            }
            catch (Exception exception)
            {
                throw new FailedToLoadDownloadedAssemblyException(commandHandlerEntity.CommandHandlerTypeAssembly,
                                                                  exception);
            }
        }

        private static MemoryStream DownloadAssemblyFromBlob(this CommandHandlerEntity commandHandlerEntity,
                                                             ICommandHandlerContext context)
        {
            var blockBlobReference = context.RegisteredCommandHandlersBlobContainer.GetBlockBlobReference(commandHandlerEntity.CommandHandlerTypeAssembly);

            var result = new MemoryStream();
            blockBlobReference.DownloadToStream(result);
            result.Seek(0,
                        SeekOrigin.Begin);

            return result;
        }

        private static ICommandHandler GetCommandHandler(this Assembly assembly,
                                                         CommandHandlerEntity commandHandlerEntity)
        {
            try
            {
                var commandHandlerType = assembly.GetExportedTypes()
                                                 .FirstOrDefault(_ => _.FullName.Equals(commandHandlerEntity.CommandHandlerTypeAssemblyQualifiedName));

                return commandHandlerType.Instantiate<ICommandHandler>();
            }
            catch (Exception exception)
            {
                throw new FailedToCreateCommandHandlerException(commandHandlerEntity.CommandHandlerTypeAssemblyQualifiedName,
                                                                exception);
            }
        }

        private static T Instantiate<T>(this Type _this)
        {
            Debug.Assert(_this != null);

            return (T) Activator.CreateInstance(_this);
        }
    }
}