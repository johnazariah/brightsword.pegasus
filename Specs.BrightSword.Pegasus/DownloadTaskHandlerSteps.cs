﻿using System;
using System.IO;

using NUnit.Framework;

using TechTalk.SpecFlow;

namespace Specs.BrightSword.Pegasus
{
    [Binding]
    public class DownloadTaskHandlerSteps
    {
        public DownloadTaskHandlerSteps()
        {
            AzureContext = ScenarioContext.Current.Get<AzureContext>("AzureContext");
            Assert.IsNotNull(AzureContext);
        }

        public AzureContext AzureContext { get; private set; }

        [Given(@"No ""(.*)"" assembly available to load")]
        public void GivenNoAssemblyAvailableToLoad(string assemblyName)
        {
            var dllPath = String.Format("{0}.dll",
                                        Path.Combine(Environment.CurrentDirectory,
                                                     assemblyName));
            if (File.Exists(dllPath))
            {
                File.Delete(dllPath);
            }

            Assert.IsFalse(File.Exists(dllPath));
        }

        [Then(@"the ""(.*)"" assembly should be downloaded")]
        public void ThenTheAssemblyShouldBeDownloaded(string assemblyName) {}
    }
}