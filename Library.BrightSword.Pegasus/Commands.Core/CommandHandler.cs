﻿using BrightSword.Pegasus.API;

using Newtonsoft.Json;

namespace BrightSword.Pegasus.Commands.Core
{
    public abstract class CommandHandler<TContext, TCommand, TCommandArgument> : ICommandHandler<TContext, TCommand, TCommandArgument>
        where TContext : ICommandHandlerContext
        where TCommand : ICommand
        where TCommandArgument : class, ICommandArgument, new()
    {
        public TContext Context { get; private set; }
        public TCommandArgument CommandArgument { get; private set; }

        void ICommandHandler<TContext, TCommand, TCommandArgument>.ProcessCommand(TCommand command,
                                                                                  TContext context)
        {
            InitializeHandler(command,
                              context);

            ProcessCommand(command,
                           context);
        }

        protected void InitializeHandler(TCommand command,
                                                 TContext context)
        {
            SetStronglyTypedContext(context);
            SetStronglyTypedCommandArgument(command);
        }

        private void SetStronglyTypedContext(TContext context)
        {
            Context = context;
        }

        private void SetStronglyTypedCommandArgument(TCommand command)
        {
            CommandArgument = JsonConvert.DeserializeObject<TCommandArgument>(command.SerializedCommandArgument);
        }

        public abstract void ProcessCommand(ICommand command,
                                            ICommandHandlerContext context);
    }
}