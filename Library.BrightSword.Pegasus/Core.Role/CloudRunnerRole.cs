﻿using System;

using BrightSword.Pegasus.API;
using BrightSword.Pegasus.API.Enumerations;
using BrightSword.Pegasus.ServiceModel.CloudRunnerCommandResult;
using BrightSword.Pegasus.ServiceModel.CommandHandler;

namespace BrightSword.Pegasus.Core.Role
{
    public abstract class CloudRunnerRole : ForeverRunningRole
    {
        protected abstract ICloudRunnerContext Context { get; }

        protected override void Action()
        {
            ProcessNextCommand(Context);
        }

        internal static void ProcessNextCommand(ICloudRunnerContext context)
        {
            var commandAndMessage = context.CommandQueue.DequeueCommand();
            if (commandAndMessage == null)
            {
                return;
            }

            try
            {
                var completionStatus = InvokeCommandHandler(commandAndMessage.Item1,
                                                            context);

                context.CommandResultsTable.EnsureInstance(new CloudRunnerCommandResult(commandAndMessage.Item1,
                                                                                        completionStatus));
            }
            catch (Exception exception)
            {
                context.CommandResultsTable.EnsureInstance(new CloudRunnerCommandResult(commandAndMessage.Item1,
                                                                                        CompletionStatus.Error_ExceptionWhileExecution,
                                                                                        exception));
            }
            finally
            {
                context.CommandQueue.DeleteCommandMessage(commandAndMessage.Item2);
            }
        }

        internal static CompletionStatus InvokeCommandHandler(ICommand command,
                                                              ICloudRunnerContext context)
        {
            if (String.IsNullOrWhiteSpace(command.CommandName))
            {
                return CompletionStatus.Warning_NoHandlerSpecified;
            }

            ICommandHandler commandHandler;
            try
            {
                commandHandler = command.GetCommandHandler(context);
            }
            catch
            {
                return CompletionStatus.Error_HandlerCouldNotBeLoaded;
            }

            try
            {
                commandHandler.ProcessCommand(command,
                                              context);

                return CompletionStatus.Completed;
            }
            catch (Exception)
            {
                return CompletionStatus.Error_HandlerNotInvokedSuccessfully;
            }
        }
    }
}