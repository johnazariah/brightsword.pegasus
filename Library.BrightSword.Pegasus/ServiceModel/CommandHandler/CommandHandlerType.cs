﻿using System;

using BrightSword.Pegasus.API.Attributes;

namespace BrightSword.Pegasus.ServiceModel.CommandHandler
{
    public class CommandHandlerType
    {
        public Type Type { get; set; }
        public RegisterCommandHandlerAttribute Attribute { get; set; }

        public string CommandName
        {
            get { return Attribute.Type.FullName; }
        }

        public string CommandHandlerTypeName
        {
            get { return Type.FullName; }
        }
    }
}