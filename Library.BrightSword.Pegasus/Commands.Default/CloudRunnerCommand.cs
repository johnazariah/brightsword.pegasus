﻿using BrightSword.Pegasus.API;

namespace BrightSword.Pegasus.Commands.Default
{
    public class CloudRunnerCommand : ICommand<string>
    {
        public string CommandArgument { get; private set; }
        public string CommandId { get; private set; }
        public string CommandName { get; private set; }
        public string CommandArgumentTypeName { get; private set; }
        public string SerializedCommandArgument { get; private set; }
    }
}