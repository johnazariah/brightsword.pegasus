﻿using System;

using Microsoft.WindowsAzure.Storage.Table;

using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Specs.BrightSword.Pegasus
{
    public static class Extensions
    {
        public static T VerifyInsertedInstance<T>(this T verifiedRow,
                                                  Table table,
                                                  Func<Table, T> customMapper = null,
                                                  Action<T, T> customComparer = null) where T : class, ITableEntity
        {
            if (customComparer == null)
            {
                table.CompareToInstance(verifiedRow);
            }
            else
            {
                var instance = customMapper == null
                                   ? table.CreateInstance<T>()
                                   : customMapper(table);

                customComparer(instance,
                               verifiedRow);
            }

            return verifiedRow;
        }
    }
}