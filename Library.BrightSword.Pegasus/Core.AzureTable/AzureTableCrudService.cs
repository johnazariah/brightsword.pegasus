﻿using System;
using System.Collections.Generic;

using BrightSword.Pegasus.API;

using Microsoft.WindowsAzure.Storage.Table;

namespace BrightSword.Pegasus.Core.AzureTable
{
    public abstract class AzureTableCrudService<TContext, TEntity>
        where TContext : class, ICloudRunnerContext
        where TEntity : TableEntity, new()
    {
        protected AzureTableCrudService(TContext context,
                                        Func<TContext, AzureTableWrapper<TEntity>> tableAccessor)
        {
            Context = context;
            TableAccessor = tableAccessor;
        }

        protected virtual AzureTableWrapper<TEntity> Table
        {
            get { return TableAccessor(Context); }
        }

        public TContext Context { get; private set; }

        public Func<TContext, AzureTableWrapper<TEntity>> TableAccessor { get; private set; }

        public IEnumerable<TEntity> GetInstances(TableQuery<TEntity> query = null)
        {
            return Table.GetInstances(query);
        }

        public TEntity GetInstanceByRowKey(string id)
        {
            return Table.RetrieveInstanceByRowKey(id);
        }

        public IEnumerable<TEntity> GetInstancesInPartition(string partitionKey)
        {
            return Table.GetInstancesInPartition(partitionKey);
        }

        public TEntity EnsureInstance(TEntity instance,
                                      bool replaceIfExists = false)
        {
            return Table.EnsureInstance(instance,
                                        replaceIfExists);
        }

        public void DeleteInstance(TEntity instance)
        {
            Table.DeleteInstance(instance);
        }
    }
}