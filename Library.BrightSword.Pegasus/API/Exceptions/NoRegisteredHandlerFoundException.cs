﻿using System;

namespace BrightSword.Pegasus.API.Exceptions
{
    public class NoRegisteredHandlerFoundException : Exception
    {
        public NoRegisteredHandlerFoundException(string taskHandlerName)
        {
            TaskHandlerName = taskHandlerName;
        }

        public string TaskHandlerName { get; set; }
    }
}