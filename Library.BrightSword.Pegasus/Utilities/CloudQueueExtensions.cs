﻿using Microsoft.WindowsAzure.Storage.Queue;

namespace BrightSword.Pegasus.Utilities
{
    public static class CloudQueueExtensions
    {
        public static CloudQueue DeleteAllMessages(this CloudQueue _this)
        {
            for (var message = _this.GetMessage();
                message != null;
                message = _this.GetMessage())
            {
                _this.DeleteMessage(message);
            }

            return _this;
        }
    }
}