﻿using System;

namespace BrightSword.Pegasus.API.Enumerations
{
    public class CanOnlySchedulePendingTasks : Exception {}
}