using BrightSword.Pegasus.Core.CommandQueue;
using BrightSword.Pegasus.ServiceModel.CloudRunnerCommandResult;

namespace BrightSword.Pegasus.API
{
    public interface ICloudRunnerContext : ICommandHandlerContext
    {
        CloudRunnerCommandResultsTable CommandResultsTable { get; }

        CloudRunnerCommandsQueue CommandQueue { get; }
    }
}