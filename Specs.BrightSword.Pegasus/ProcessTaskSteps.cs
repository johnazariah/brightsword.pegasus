﻿using Microsoft.WindowsAzure.Storage.Queue;

using Newtonsoft.Json;

using NUnit.Framework;

using TechTalk.SpecFlow;

namespace Specs.BrightSword.Pegasus
{
    [Binding]
    public class ProcessTaskSteps
    {
        private CloudQueueMessage _scheduledTaskMessage;

        public ProcessTaskSteps()
        {
            AzureContext = ScenarioContext.Current.Get<AzureContext>("AzureContext");
            Assert.IsNotNull(AzureContext);
        }

        private AzureContext AzureContext { get; set; }

        [When(@"A ScheduledTask is dequeued from ScheduledCloudRunnerTasks")]
        public void WhenAScheduledTaskIsDequeuedFromScheduledCloudRunnerTasks()
        {
            _scheduledTaskMessage = AzureContext.ScheduledTasksQueue.GetMessage();

            var actual = JsonConvert.DeserializeObject<CloudRunnerTask>(_scheduledTaskMessage.AsString);
            Assert.AreEqual(AzureContext.Sample.PartitionKey,
                            actual.PartitionKey);
            Assert.AreEqual(AzureContext.Sample.RowKey,
                            actual.RowKey);
        }

        [When(@"the TaskProcess invokes the process operation on the ScheduledTask")]
        public void WhenTheTaskProcessInvokesTheProcessOperationOnTheScheduledTask()
        {
            RoleBase.ProcessTask(_scheduledTaskMessage,
                                 AzureContext);
        }

        [Then(@"The ScheduledTask should not exist in ScheduledCloudRunnerTasks any more")]
        public void ThenTheScheduledTaskShouldNotExistInScheduledCloudRunnerTasksAnyMore()
        {
            var message = AzureContext.ScheduledTasksQueue.GetMessage();
            Assert.IsNull(message);
        }
    }
}