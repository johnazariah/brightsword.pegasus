﻿using BrightSword.Pegasus.API;
using BrightSword.Pegasus.Core;
using BrightSword.Pegasus.Core.CommandQueue;
using BrightSword.Pegasus.Properties;
using BrightSword.Pegasus.ServiceModel.CloudRunnerCommandResult;

namespace BrightSword.Pegasus.Configuration
{
    public class CloudRunnerContext : CommandHandlerContext,
                                      ICloudRunnerContext
    {
        private readonly CloudRunnerCommandResultsTable _runtimeErrorsTable;

        private readonly CloudRunnerCommandsQueue _scheduledTasksQueue;

        public CloudRunnerContext()
            : this(CloudRunnerConfiguration.CreateFromApplicationSettings()) {}

        public CloudRunnerContext(ICloudRunnerConfiguration configuration)
            : base(configuration)
        {
            _scheduledTasksQueue = new CloudRunnerCommandsQueue(CloudStorageAccount,
                                                                configuration.CommandQueueName);
            _runtimeErrorsTable = new CloudRunnerCommandResultsTable(CloudStorageAccount,
                                                                     configuration.CommandResultsTableName);
        }

        public static CloudRunnerContext CreateFromApplicationSettings()
        {
            return new CloudRunnerContext(CloudRunnerConfiguration.CreateFromApplicationSettings());
        }

        public CloudRunnerCommandResultsTable CommandResultsTable
        {
            get { return _runtimeErrorsTable; }
        }

        public CloudRunnerCommandsQueue CommandQueue
        {
            get { return _scheduledTasksQueue; }
        }
    }

    internal class CloudRunnerConfiguration : ICloudRunnerConfiguration
    {
        public string StorageAccount { get; set; }

        public string StorageAccountKey { get; set; }

        public string RegisteredCommandHandlersTableName { get; set; }

        public string RegisteredCommandHandlersBlobContainerName { get; set; }

        public string CommandQueueName { get; set; }

        public string CommandResultsTableName { get; set; }

        public static CloudRunnerConfiguration CreateFromApplicationSettings()
        {
            return new CloudRunnerConfiguration
            {
                StorageAccount = Settings.Default.StorageAccount,
                StorageAccountKey = Settings.Default.StorageAccountKey,
                RegisteredCommandHandlersTableName = Settings.Default.RegisteredCommandHandlersTableName,
                RegisteredCommandHandlersBlobContainerName = Settings.Default.RegisteredCommandHandlersBlobContainerName,
                CommandQueueName = Settings.Default.CommandQueueName,
                CommandResultsTableName = Settings.Default.CommandResultsTableName
            };
        }
    }
}