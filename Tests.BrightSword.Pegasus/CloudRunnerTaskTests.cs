﻿using BrightSword.Crucible;
using BrightSword.Pegasus.API;
using BrightSword.Pegasus.API.Enumerations;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.BrightSword.Pegasus
{
    [TestClass]
    public class CloudRunnerTaskTests
    {
        [TestMethod]
        public void ExceptionIsThrown_When_Scheduling_ANonPendingTask()
        {
            ExceptionHelper.ExpectException<CanOnlySchedulePendingTasks>(() => RoleBase.ScheduleTask(new CloudRunnerTask
                                                                                                     {
                                                                                                         CompletionStatus = CompletionStatus.Completed.ToString()
                                                                                                     },
                                                                                                     null));

            ExceptionHelper.ExpectException<CanOnlySchedulePendingTasks>(() => RoleBase.ScheduleTask(new CloudRunnerTask
                                                                                                     {
                                                                                                         CompletionStatus = CompletionStatus.Queued.ToString()
                                                                                                     },
                                                                                                     null));
        }
    }
}