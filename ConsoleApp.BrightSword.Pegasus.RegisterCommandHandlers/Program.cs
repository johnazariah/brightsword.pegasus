﻿using System;
using System.IO;
using System.Reflection;

using BrightSword.Pegasus.Configuration;
using BrightSword.Pegasus.ServiceModel.CommandHandler;
using BrightSword.SwissKnife;

using Microsoft.WindowsAzure.Storage.Blob;

namespace ConsoleApp.BrightSword.Pegasus.RegisterCommandHandlers
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var parameters = new Parameters
                             {
                                 FullAssemblyPath = args.GetArgumentValue("--assembly",
                                                                          _ => _)
                             };

            var context = new CloudRunnerContext();

            var assembly = Assembly.LoadFrom(parameters.FullAssemblyPath);
            if (assembly == null)
            {
                throw new ArgumentException(String.Format("Unable to load assembly {0}",
                                                          parameters.FullAssemblyPath));
            }

            var commandHandlerTypes = assembly.GetCommandHandlerTypes();

            var assemblyName = Path.GetFileNameWithoutExtension(assembly.CodeBase);

            foreach (var commandHandlerType in commandHandlerTypes)
            {
                context.RegisteredCommandHandlersTable.EnsureInstance(new CommandHandlerEntity(commandHandlerType.CommandName,
                                                                                               assemblyName,
                                                                                               commandHandlerType.CommandHandlerTypeName));
            }

            context.RegisteredCommandHandlersBlobContainer.SetPermissions(new BlobContainerPermissions
                                                                          {
                                                                              PublicAccess = BlobContainerPublicAccessType.Container
                                                                          });

            var blob = context.RegisteredCommandHandlersBlobContainer.GetBlockBlobReference(assemblyName);
            blob.DeleteIfExists();

            using (var stream = new FileStream(parameters.FullAssemblyPath,
                                               FileMode.Open,
                                               FileAccess.Read,
                                               FileShare.Read))
            {
                blob.UploadFromStream(stream);
            }
        }

        private class Parameters
        {
            public string FullAssemblyPath { get; set; }
        }
    }
}