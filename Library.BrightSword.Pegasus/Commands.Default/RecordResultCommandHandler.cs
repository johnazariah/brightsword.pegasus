﻿using BrightSword.Pegasus.API;
using BrightSword.Pegasus.Commands.Core;
using BrightSword.Pegasus.Configuration;

using Microsoft.WindowsAzure.Storage.Table;

namespace BrightSword.Pegasus.Commands.Default
{
    public class RecordResultCommandHandler : CommandHandler<CloudRunnerContext, RecordResultCommand, RecordResultCommandArgument>
    {
        public override void ProcessCommand(ICommand command,
                                            ICommandHandlerContext context)
        {
            var resultsTable = new CloudTableClient(context.CloudStorageAccount.TableEndpoint,
                                                    context.CloudStorageAccount.Credentials).GetTableReference(CommandArgument.ResultTableName);
            resultsTable.CreateIfNotExists();

            resultsTable.Execute(TableOperation.Insert(CommandArgument.Entity));
        }
    }
}